import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GastosTotaisComponent } from './gastos-totais.component';

describe('GastosTotaisComponent', () => {
  let component: GastosTotaisComponent;
  let fixture: ComponentFixture<GastosTotaisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GastosTotaisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GastosTotaisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
