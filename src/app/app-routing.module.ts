import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GastosComponent } from './gastos/gastos.component';
import {GastosTotaisComponent} from './gastos-totais/gastos-totais.component';


const routes: Routes = [
  { path: 'gastos', component: GastosComponent },
  { path: 'gastosTotais',  component: GastosTotaisComponent},
  { path: '', redirectTo: '/gastos', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
