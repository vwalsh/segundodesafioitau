import { Component, OnInit } from '@angular/core';
import {GastosService} from '../services/gastos.service';
import {Lancamento} from '../models/lancamento.model';
import {Categoria} from '../models/categoria.model';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-gastos',
  templateUrl: './gastos.component.html',
  styleUrls: ['./gastos.component.scss']
})
export class GastosComponent implements OnInit {
  lancamentos: Lancamento[];
  categorias: Categoria[];

  constructor(
    private api: GastosService ) { }

  ngOnInit() {
    this.getCategoria();
    this.getLancamentos();
  }

  getCategoria() {
    this.api.getCategorias().subscribe((categorias: Categoria[]) => {
      this.categorias = categorias;
    });
  }

  getLancamentos() {
    this.api.getLancamentos().subscribe((lancamentos: Lancamento[]) => {
      this.lancamentos = lancamentos;
    });
  }

  changeCategory(valor) {
    return this.categorias.find( x => x.id === valor).nome;
  }
}
