import { Injectable, EventEmitter } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {retry, catchError, tap} from 'rxjs/operators';
import { Lancamento} from '../models/lancamento.model';
import { Categoria} from '../models/categoria.model';

@Injectable({
    providedIn: 'root'
})
export class GastosService {

  private readonly API = 'https://desafio-it-server.herokuapp.com/'
  constructor(private httpClient: HttpClient) { }

  // Headers
  httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  getLancamentos(): Observable<Lancamento[]> {
      return this.httpClient.get<Lancamento[]>(this.API + 'lancamentos')
        .pipe(
          retry(2),
          catchError(this.handleError));
    }

  // Listar Categorias
  getCategorias(): Observable<Categoria[]> {
    return this.httpClient.get<Categoria[]>(this.API + 'categorias')
      .pipe(
        retry(2),
        catchError(this.handleError));
  }

  // Manipulação de erros
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Erro ocorreu no lado do client
      errorMessage = error.error.message;
    } else {
      // Erro ocorreu no lado do servidor
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    return throwError(errorMessage);
  }

}
