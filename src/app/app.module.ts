import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GastosComponent } from './gastos/gastos.component';
import {HttpClientModule} from '@angular/common/http';
import {MonthPipe} from './pipes/month.pipe';
import {OrderModule} from 'ngx-order-pipe';
import { GastosTotaisComponent } from './gastos-totais/gastos-totais.component';

@NgModule({
  declarations: [
    AppComponent,
    GastosComponent,
    MonthPipe,
    GastosTotaisComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        OrderModule
    ],
  providers: [MonthPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
